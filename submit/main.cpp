#include <iostream>
#include <thread>
#include <chrono>
#include <optional>
#include <exception>

#include <experimental/coroutine>

#include "../support/unit.hpp"
#include "../support/functor_traits.hpp"
#include "../support/coro.hpp"

using namespace std::chrono_literals;

using std::experimental::coroutine_handle;

//////////////////////////////////////////////////////////////////////

// Concepts

// ~ Callback
template <typename R, typename T>
concept ReceiverOf = requires (R r, T value) {
  r.SetValue(value);
  r.SetError(std::exception_ptr{});
  r.SetDone();
};

namespace detail {

template <typename T>
struct DummyReceiverOf {
  void SetValue(T) {}
  void SetError(std::exception_ptr) {}
  void SetDone() {}
};

}  // namespace detail

template <typename S>
concept Sender = requires (S s) {
  typename S::ValueType;
  s.Submit(detail::DummyReceiverOf<typename S::ValueType>{});
};

//////////////////////////////////////////////////////////////////////

// AsReceiver

namespace detail {

template<typename F>
struct FunctorReceiver {
  using T = typename support::FunctorTraits<F>::ArgumentType;

  FunctorReceiver(F f) : functor(f) {
  }

  void SetValue(T value) {
    functor(value);
  }

  void SetError(std::exception_ptr) {
    std::abort();  // Not implemented
  }

  void SetDone() {
    std::abort();  // Not implemented
  }

  F functor;
};

}  // namespace detail

template <typename F>
auto AsReceiver(F f) {
  return detail::FunctorReceiver<F>(f);
}

//////////////////////////////////////////////////////////////////////

// `Just` sender

namespace detail {

template<typename T>
struct JustSender {
  using ValueType = T;

  template <typename R> requires ReceiverOf<R, T>
  void Submit(R receiver) {
    receiver.SetValue(value);
  }

  T value;
};

}  // namespace detail

template <typename T>
auto Just(T value) {
  return detail::JustSender<T>{value};
}

//////////////////////////////////////////////////////////////////////

// `NewThread` sender

namespace detail {

struct NewThreadSender {
  using ValueType = Unit;

  template<typename R> requires ReceiverOf<R, Unit>
  void Submit(R receiver) {
    std::thread([receiver]() mutable {
      receiver.SetValue(Unit{});
    }).detach();
  }
};

}  // namespace detail

auto NewThread() {
  return detail::NewThreadSender{};
}

//////////////////////////////////////////////////////////////////////

// `ThreadPool` sender

class ThreadPool {
 public:
  using Task = std::function<void()>;

  struct Sender {
    ThreadPool& tp;

    using ValueType = Unit;

    template <typename R>
    void Submit(R receiver) {
      tp.Submit([receiver]() mutable {
        receiver.SetValue(Unit{});
      });
    }
  };

  struct Scheduler {
    ThreadPool& tp;

    auto Schedule() {
      return ThreadPool::Sender{tp};
    }
  };

  Scheduler GetScheduler() {
    return {*this};
  }

 private:
  void Submit(Task task) {
    std::cout << "Run task" << std::endl;
    task();
  }
};

//////////////////////////////////////////////////////////////////////

// Then combinator

// t_sender - produces T
// cont - U(T)
// ThenSender - produces U

namespace detail {

template<typename S, typename F>
struct ThenSender {
  using T = typename S::ValueType;
  using U = typename support::FunctorTraits<F>::ReturnType;

  using ValueType = U;

  template<typename R> requires ReceiverOf<R, U>
  void Submit(R u_receiver) {
    auto t_receiver = AsReceiver([cont = cont, u_receiver](T t_value) mutable {
      try {
        U u_value = cont(t_value);
        u_receiver.SetValue(u_value);
      } catch (...) {
        u_receiver.SetError(std::current_exception());
      }
    });
    t_sender.Submit(t_receiver);
  }

  S t_sender;
  F cont;
};

}  // namespace detail

template <typename S, typename F>
auto Then(S sender, F cont) {
  return detail::ThenSender<S, F>{sender, cont};
}

template <typename S, typename F>
auto operator | (S sender, F cont) {
  return Then(sender, cont);
}

//////////////////////////////////////////////////////////////////////

namespace detail {

template <typename Scheduler, typename S>
struct OnSender {
  using ValueType = typename S::ValueType;

  template <typename R>
  void Submit(R receiver) {
    auto t_receiver = AsReceiver([sched_sender = sched.Schedule(), r = receiver](ValueType v) mutable {
      auto sched_receiver = AsReceiver([r, v](Unit) mutable {
        r.SetValue(v);
      });
      sched_sender.Submit(sched_receiver);
    });
    sender.Submit(t_receiver);
  }

  Scheduler sched;
  S sender;
};

}

template <typename Scheduler, typename S>
auto On(Scheduler sched, S s) {
  return detail::OnSender<Scheduler, S>{sched, s};
}

//////////////////////////////////////////////////////////////////////

// Generic operations

template <typename S, typename R>
auto Submit(S& sender, R receiver) {
  sender.Submit(receiver);
}

//////////////////////////////////////////////////////////////////////

// Coroutine integration

// Receiver ~ Coroutine
// Sender ~ Awaiter
// Submit ~ await_suspend

namespace detail {

template <Sender S>
struct SenderAwaiter {
  S sender;

  using T = typename S::ValueType;

  std::optional<T> value;

  bool await_ready() {
    return false;
  }

  void await_suspend(std::experimental::coroutine_handle<> h) {
    sender.Submit(AsReceiver([h, this](T v) mutable {
      value.emplace(v);
      h.resume();
    }));
  }

  T await_resume() {
    return *value;
  }
};

}

template <Sender S>
auto operator co_await(S sender) {
  return detail::SenderAwaiter<S>{sender};
}

template <typename Scheduler>
std::future<void> Coro(Scheduler scheduler) {
  co_await scheduler.Schedule();
  int value = co_await Just(17);
  std::cout << "Value = " << value << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Receiver

  {
    auto f = [](int value) {
      std::cout << "Received value " << value << std::endl;
    };
    auto r = AsReceiver(f);
    r.SetValue(17);
  }

  // Just + Submit

  {
    auto sender = Just(15);
    auto receiver = AsReceiver([](int value) {
      std::cout << "Just value: " << value << std::endl;
    });

    Submit(sender, receiver);
  }

  // NewThread

  {
    auto sender = NewThread();
    auto receiver = AsReceiver([](Unit) {
      std::cout << "Running at thread = "
                << std::this_thread::get_id() << std::endl;
    });
    Submit(sender, receiver);

    std::this_thread::sleep_for(1s);
  }

  // ThreadPool

  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();

    auto sender = tp_scheduler.Schedule();

    auto receiver = AsReceiver([](Unit) {
      std::cout << "Running at thread pool!" << std::endl;
    });

    Submit(sender, receiver);
  }

  // ThreadPool + Then

  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();

//    auto s0 = ScheduleTo(tp);
//    auto s1 = Then(s0, [](Unit) {
//      return 42;
//    });
//    auto s2 = Then(s1, [](int v) {
//      return v + 1;
//    });

    auto s = tp_scheduler.Schedule() |
        [](Unit) { return 42; } |
        [](int v) { return v + 1; };

    auto receiver = AsReceiver([](int value) {
      std::cout << "Pipeline result: " << value << std::endl;
    });
    Submit(s, receiver);
  }

  // On

  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();

    auto s0 = Just(7);
    auto s1 = Then(s0, [](int v) {
      return v + 1;
    });
    auto s2 = Then(s1, [](int v) {
      return v * 2;
    });

    auto s3 = On(tp_scheduler, s2);

    auto receiver = AsReceiver([](int value) {
      std::cout << "Pipeline result: " << value << std::endl;
    });

    Submit(s3, receiver);
  }

  // Coroutine

  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();
    Coro(tp_scheduler);
  }

  return 0;
}
