#include <iostream>

#include <experimental/coroutine>

#include <type_traits>
#include <thread>
#include <chrono>
#include <optional>
#include <functional>
#include <queue>

#include "../support/coro.hpp"
#include "../support/unit.hpp"
#include "../support/functor_traits.hpp"

using namespace std::chrono_literals;

using std::experimental::coroutine_handle;

//////////////////////////////////////////////////////////////////////

// Concepts

// ~ Callback
template <typename R, typename T>
concept ReceiverOf = requires (R r, T value) {
  r.SetValue(value);
  r.SetError(std::exception_ptr{});  // Error handling
  r.SetDone();  // Cancellation
};

namespace detail {

template <typename T>
struct DummyReceiverOf {
  void SetValue(T) {}
  void SetError(std::exception_ptr) {}
  void SetDone() {}
};

}  // namespace detail

template <typename S>
concept Sender = requires (S s) {
  typename S::ValueType;
  s.Connect(detail::DummyReceiverOf<typename S::ValueType>{});
};

//////////////////////////////////////////////////////////////////////

// Receiver

namespace detail {

template<typename F>
struct FunctorReceiver {
  using T = typename support::FunctorTraits<F>::ArgumentType;

  FunctorReceiver(F f) : functor(f) {
  }

  void SetValue(T value) {
    functor(value);
  }

  // Error handling
  void SetError(std::exception_ptr) {
    std::abort();  // Not implemented
  }

  // Cancellation
  void SetDone() {
    std::abort();  // Not implemented
  }

  F functor;
};

}  // namespace detail

template <typename F>
auto AsReceiver(F f) {
  return detail::FunctorReceiver<F>(f);
}

//////////////////////////////////////////////////////////////////////

// ~ std::function<void(void)>
class Function {
  struct IInvokable {
    virtual ~IInvokable() = default;
    virtual void Invoke() = 0;
  };

  template <typename F>
  struct FunctionStorage : IInvokable {
    F f;

    FunctionStorage(F fun) : f(fun) {}

    void Invoke() override {
      f();
    }
  };

 public:
  template <typename F>
  Function(F f) {
    invokable_ = new FunctionStorage<F>(f);
  }

  void operator ()() {
    invokable_->Invoke();
  }

  ~Function() {
    delete invokable_;
  }

 private:
  IInvokable* invokable_;
};

//////////////////////////////////////////////////////////////////////

// `ThreadPool` sender

class ThreadPool {
 public:
  // using Task = std::function<void()>;

  struct ITask {
    virtual ~ITask() = default;

    virtual void Run() = 0;
  };

  struct TaskBase : ITask {
    TaskBase* next = nullptr;
  };

  template <typename R>
  struct ManualOperationState : TaskBase {
    R receiver;
    ThreadPool& tp;

    ManualOperationState(ThreadPool& pool, R r) : receiver(r), tp(pool) {
    }

    void Run() override {
      receiver.SetValue(Unit{});
    }

    void Start() {
      tp.Submit(this);
    }
  };

  template <typename R>
  struct AutomaticOperationState : TaskBase {
    R receiver;

    AutomaticOperationState(R r) : receiver(r) {
    }

    void Run() override {
      receiver.SetValue(Unit{});
      delete this;
    }
  };

  struct Sender {
    using ValueType = Unit;

    ThreadPool& tp;

    template <typename R>
    auto Connect(R r) {
      return ManualOperationState(tp, r);
    }

    template <typename R>
    void Spawn(R r) {
      auto* op = new AutomaticOperationState{r};
      tp.Submit(op);
    }
  };

  struct Scheduler {
    ThreadPool& tp;

    auto Schedule() {
      return Sender{tp};
    }
  };

  Scheduler GetScheduler() {
    return Scheduler{*this};
  }

  void RunTasks() {
    while (head_ != nullptr) {
      TaskBase* next = head_;
      head_ = head_->next;
      std::cout << "Run task" << std::endl;
      next->Run();
    }
  }

 private:
  void Submit(TaskBase* task) {
    task->next = head_;
    head_ = task;
  }

 private:
  TaskBase* head_ = nullptr;
};

//////////////////////////////////////////////////////////////////////

// Generic operations

//template <typename S, typename R>
//void Submit(S& sender, R receiver) {
//  sender.Submit(receiver);
//}

template <typename S, typename R>
auto Connect(S& sender, R receiver) {
  return sender.Connect(receiver);
}

template <typename Op>
void Start(Op& op) {
  op.Start();
}

template <typename S, typename R>
void Spawn(S& sender, R receiver) {
  sender.Spawn(receiver);
}

//////////////////////////////////////////////////////////////////////

// Coroutine integration

// Receiver - Coroutine
// Sender - Awaiter
// Submit - await_suspend
// Operation state - Coroutine state

namespace detail {

struct ThreadPoolAwaiter {
  ThreadPool::Sender sender;

  bool await_ready() {
    return false;
  }

  struct CoroReceiver {
    std::experimental::coroutine_handle<> h;

    void SetValue(Unit) {
      h.resume();
    }

    void SetError(std::exception_ptr) {}
    void SetDone() {}
  };

  using OpState = ThreadPool::ManualOperationState<CoroReceiver>;

  std::optional<OpState> op_;

  void await_suspend(std::experimental::coroutine_handle<> h) {
    op_.emplace(sender.Connect(CoroReceiver{h}));
    Start(*op_);
  }

  void await_resume() {
  }
};

}

auto operator co_await(ThreadPool::Sender sender) {
return detail::ThreadPoolAwaiter{sender};
}

template <typename Scheduler>
std::future<void> Coro(Scheduler scheduler) {
  co_await scheduler.Schedule();
  std::cout << "Hi from coroutine" << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Function
  {
    auto l1 = []() {
      std::cout << "Hi" << std::endl;
    };

    int x = 7;
    auto l2 = [x]() {
      std::cout << "x = " << x << std::endl;
    };

    std::cout << "sizeof(l1) = " << sizeof(l1) << std::endl;
    std::cout << "sizeof(l2) = " << sizeof(l2) << std::endl;

    Function f1(l1);
    Function f2(l2);

    f1();
    f2();
  }

  // Connect + Start

  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();

    auto sender = tp_scheduler.Schedule();
    auto receiver = AsReceiver([](Unit) {
      std::cout << "Hi from receiver" << std::endl;
    });

    auto op = Connect(sender, receiver);
    Start(op);

    Spawn(sender, receiver);

    tp.RunTasks();
  }

  // Coroutine

  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();

    Coro(tp_scheduler);

    tp.RunTasks();
  }

  return 0;
}
